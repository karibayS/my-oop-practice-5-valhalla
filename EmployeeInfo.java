package com.company;

import java.sql.*;
import java.util.Scanner;

public class EmployeeInfo extends User implements DatabaseCommand{ // this class is extended of User class
    Scanner scanner = new Scanner(System.in);

    public EmployeeInfo(int id, String firstname, String lastname, String email, String phone_number, String country) { // user class has a constructor
        super(id, firstname, lastname, email, phone_number, country); // which fills information
    }

    public void insert()
    {

        String sql = "INSERT INTO employee_information (id, firstname, lastname, email ,phone_number, country) VALUES (?, ?, ?, ?, ?, ?)";

        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/valhalla", "root", "root");

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, super.id);
            statement.setString(2, firstname);
            statement.setString(3, lastname);
            statement.setString(4, email);
            statement.setString(5, phone_number);
            statement.setString(6, country);

            int rows = statement.executeUpdate();

            if (rows > 0) {
                System.out.println("A new employee_information has been inserted");
            }

            connection.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }



    @Override
    public void select() {
        String sql = "SELECT * FROM employee_information";
        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/valhalla", "root","root");

            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            while (result.next()){
                System.out.println(
                        result.getInt(1) + " " + result.getString(2) + " " + result.getString(3) + " "
                                + result.getString(4) + " " + result.getString(5) + " " + result.getString(6)
                );
            }

            connection.close();

        } catch (SQLException ex) {
            ex.printStackTrace() ;
        }
    }



    @Override
    public void delete() {
        String sql = "DELETE FROM `employee_information` WHERE `employee_information`.`id` = ?";
        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/valhalla", "root","root");

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1,id);

            int rows = statement.executeUpdate();

            if(rows > 0) {
                System.out.println("The employee_information has been deleted");
            }


            connection.close();

        } catch (SQLException ex) {
            ex.printStackTrace() ;
        }

    }

    @Override
    public void update() {
        String sql = "UPDATE employee_information SET firstname=?, lastname=?, email=? ,phone_number=?, country=? WHERE id=?";
        String firstname = scanner.nextLine();
        String lastname = scanner.nextLine();
        String email = scanner.nextLine();
        String phone_number = scanner.nextLine();
        String country = scanner.nextLine();

        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/valhalla", "root","root");

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(6,id);
            statement.setString(1,firstname);
            statement.setString(2,lastname);
            statement.setString(3,email);
            statement.setString(4,phone_number);
            statement.setString(5,country);

            int rows = statement.executeUpdate();

            if(rows > 0) {
                System.out.println("The employee_information has been updated");
            }

            connection.close();

        } catch (SQLException ex) {
            ex.printStackTrace() ;
        }
    }
}

