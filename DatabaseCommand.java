package com.company;

public interface DatabaseCommand { // interface, which has 4 methods: insert,select,update,delete
    // and every object will execute it by it's own
    public void insert();
    public void select();
    public void delete();
    public void update();
}
