package com.company;
import java.util.Scanner;
import java.sql.*;

public class Responsibilities implements DatabaseCommand{
    Scanner scanner = new Scanner(System.in);

    public void insert() {
        int id = scanner.nextInt();
        scanner.nextLine();
        String job = scanner.nextLine();
        String sql = "INSERT INTO responsibilities (id, job) VALUES (?, ?)";

        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/valhalla", "root", "root");

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.setString(2, job);

            int rows = statement.executeUpdate();

            if (rows > 0) {
                System.out.println("A new responsibility has been inserted");
            }

            connection.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    public void select() {
        String sql = "SELECT * FROM responsibilities";
        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/valhalla", "root","root");

            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            while (result.next()){
                System.out.println(result.getInt(1) + " " + result.getString(2));
            }

            connection.close();

        } catch (SQLException ex) {
            ex.printStackTrace() ;
        }
    }

    public void update() {
        int id = scanner.nextInt();
        scanner.nextLine();
        String job = scanner.nextLine();
        String sql = "UPDATE responsibilities SET job=? WHERE id=?";

        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/valhalla", "root","root");

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(2,id);
            statement.setString(1,job);

            int rows = statement.executeUpdate();

            if(rows > 0) {
                System.out.println("The responsibility has been updated");
            }

            connection.close();

        } catch (SQLException ex) {
            ex.printStackTrace() ;
        }
    }

    public void delete() {
        int id = scanner.nextInt();
        String sql = "DELETE FROM responsibilities WHERE id=?";
        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/valhalla", "root","root");

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1,id);

            int rows = statement.executeUpdate();

            if(rows > 0) {
                System.out.println("The responsibility has been deleted");
            }


            connection.close();

        } catch (SQLException ex) {
            ex.printStackTrace() ;
        }
    }
}
