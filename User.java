package com.company;

public abstract class User { // user is the same like table EmployeeInfo, which has 6 columns: id, fname,lname,email,phone,country
    // but we cannot create an object by this class,because we need to use EmployeeInfo class like an object
    int id;
    String firstname;
    String lastname;
    String email;
    String phone_number;
    String country;

    public User(int id,String firstname, String lastname, String email, String phone_number, String country) { // constructor
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.phone_number = phone_number;
        this.country = country;
    }



}
