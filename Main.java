package com.company;

import java.sql.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        EmployeeInfo employeeInfo = new EmployeeInfo(15,"Connor","Stark", // creating employee user, and inputting info
                "houseofstark@mail.ru","+7 568-912-0560","Norway");
        Position position = new Position(); // position object, id 1 -> project head
        Salary salary = new Salary(); // salary object, position id 1 has salary 8000$
        Employee employee = new Employee(); // employee object, it shows what position has every employee
        Responsibilities responsibilities = new Responsibilities(); // responsibility for every employee

        DatabaseQuery databaseQuery = new DatabaseQuery(); // database querry has 4 method,which calls every object's querry
        databaseQuery.select(salary); // for instance, selecting the whole info about salaries

        Project website = new Project(); // creating an object 'project'
        website.setProject_name("Create a new Website"); // giving a name
        website.getProject_name();
        System.out.println(website.costOfProject(1,3,0,0,2));
        // here we have to put how many and what exactly positions will work with that project

    }

}








