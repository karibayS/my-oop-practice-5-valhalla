package com.company;
import java.sql.*;
import java.util.Scanner;

public class Position implements DatabaseCommand {
    Scanner scanner = new Scanner(System.in); // creating a scanner object

    public void insert() {
        int id = scanner.nextInt();
        scanner.nextLine();
        String name = scanner.nextLine();
        String sql = "INSERT INTO positions (id, name) VALUES (?, ?)"; // sql command

        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/valhalla", "root", "root"); // connection to database

            PreparedStatement statement = connection.prepareStatement(sql); // sending the sql command
            statement.setInt(1, id); // inserting new values
            statement.setString(2, name);

            int rows = statement.executeUpdate(); // executing code while it counts how many rows are inserted

            if (rows > 0) { // if it's inserted
                System.out.println("A new position has been inserted"); // then we have this system out
            }

            connection.close();// closing connection

        } catch (SQLException ex) { // if there are some mistakes, we catch it
            ex.printStackTrace(); // and outputting it to the console
        }

    }

    public void select() {
        String sql = "SELECT * FROM positions";
        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/valhalla", "root","root");

            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            while (result.next()){
                System.out.println(result.getInt(1) + " " + result.getString(2));
            }

            connection.close();

        } catch (SQLException ex) {
            ex.printStackTrace() ;
        }
    }

    public void update() {
        int id = scanner.nextInt();
        scanner.nextLine();
        String name = scanner.nextLine();
        String sql = "UPDATE positions SET name=? WHERE id=?";

        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/valhalla", "root","root");

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(2,id);
            statement.setString(1,name);

            int rows = statement.executeUpdate();

            if(rows > 0) {
                System.out.println("The position has been updated");
            }

            connection.close();

        } catch (SQLException ex) {
            ex.printStackTrace() ;
        }
    }

    public void delete() {
        int id = scanner.nextInt();
        String sql = "DELETE FROM positions WHERE id=?";
        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/valhalla", "root","root");

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1,id);

            int rows = statement.executeUpdate();

            if(rows > 0) {
                System.out.println("The position has been deleted");
            }


            connection.close();

        } catch (SQLException ex) {
            ex.printStackTrace() ;
        }
    }

}


