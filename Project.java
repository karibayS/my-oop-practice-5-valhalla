package com.company;

import java.sql.*;

public class Project{

    private String project_name;

    public double costOfProject // method,which accepts 5 values, which are numbers of every employee's position, and then
            // it multiplies to their salaries, and counts the cost of the project
            (int numberOfHead, int numberOfSenEngId,int numberOfJunEng, int numberOfWebDev, int numberOfNetwAdmin)
    {
        return
                (8000 * numberOfHead) + (7500 * numberOfSenEngId) + (6000 * numberOfJunEng) +
                        (6500 * numberOfWebDev) +(5000 * numberOfNetwAdmin);
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }


}
