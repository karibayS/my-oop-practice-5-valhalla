-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 21, 2021 at 10:19 AM
-- Server version: 5.7.24
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `valhalla`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `position_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `position_id`) VALUES
(1, 1),
(2, 2),
(3, 2),
(5, 3),
(6, 3),
(7, 3),
(8, 4),
(9, 4),
(10, 5),
(11, 5),
(12, 5),
(13, 5);

-- --------------------------------------------------------

--
-- Table structure for table `employee_information`
--

CREATE TABLE `employee_information` (
  `id` int(11) NOT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone_number` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee_information`
--

INSERT INTO `employee_information` (`id`, `firstname`, `lastname`, `email`, `phone_number`, `country`) VALUES
(1, 'Evan', 'Bryanna', 'dwall@hotmail.com', '+7 314-898-3477', 'England'),
(2, 'Susann', 'Indiana', 'tscott@gmail.com', '+1-202-555-0135', 'Brazil'),
(3, 'Cat', 'Huxley', 'karengaines@gmail.com', '+3 520-823-2401', 'USA'),
(4, 'Alexism', 'Dot', 'fbrown@gmail.com', '+4 210-865-6817', 'Norway'),
(5, 'Katheryne', 'Zita', 'andrewwade@gmail.com', '+5 320-473-9924', 'Norway'),
(6, 'Satchel', 'Carlyn', 'aaronmartinez@gonzalez.com', '+3 223-234-0267', 'USA'),
(7, 'Cohe', 'Dayna', 'crawfordwilliam@bowers.com', '+4 423-953-6789', 'Norway'),
(8, 'Nathalie', 'Paulette', 'dillonmartin@hotmail.com', '+9 406-501-2828', 'France'),
(9, 'Fancy', 'Posie', 'mark35@martinez.com', '+5 712-359-9220', 'France'),
(10, 'Gabin', 'Dwayne', 'fbrown@yahoo.com', '+6 207-577-5192', 'Italy'),
(11, 'Colene', 'Elisabeth', 'leahlee@jones.com', '+2 612-452-9619', 'USA'),
(12, 'Nicodème', 'Romaine', 'morgan27@yahoo.com', '+7 225-964-5804', 'USA'),
(13, 'Lawrence', 'Elfriede', 'brian69@larson-taylor.com', '+8 229-622-9050', 'USA'),
(15, 'Connor', 'Stark', 'houseofstark@mail.ru', '+7 568-912-0560', 'Norway');

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `name`) VALUES
(1, 'Project head  '),
(2, 'Senior engineer'),
(3, 'Junior engineer'),
(4, 'Web developer'),
(5, 'Network administrator'),
(6, 'Karik');

-- --------------------------------------------------------

--
-- Table structure for table `responsibilities`
--

CREATE TABLE `responsibilities` (
  `id` int(11) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE `salary` (
  `id` int(11) DEFAULT NULL,
  `sum` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `salary`
--

INSERT INTO `salary` (`id`, `sum`) VALUES
(1, 8000),
(2, 7500),
(3, 6000),
(4, 6500),
(5, 5000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `position_id` (`position_id`);

--
-- Indexes for table `employee_information`
--
ALTER TABLE `employee_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `responsibilities`
--
ALTER TABLE `responsibilities`
  ADD KEY `id` (`id`);

--
-- Indexes for table `salary`
--
ALTER TABLE `salary`
  ADD KEY `id` (`id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`id`) REFERENCES `employee_information` (`id`),
  ADD CONSTRAINT `employees_ibfk_2` FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`);

--
-- Constraints for table `responsibilities`
--
ALTER TABLE `responsibilities`
  ADD CONSTRAINT `responsibilities_ibfk_1` FOREIGN KEY (`id`) REFERENCES `employees` (`id`);

--
-- Constraints for table `salary`
--
ALTER TABLE `salary`
  ADD CONSTRAINT `salary_ibfk_1` FOREIGN KEY (`id`) REFERENCES `positions` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
