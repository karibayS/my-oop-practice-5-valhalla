package com.company;

public class DatabaseQuery {
    public void select(DatabaseCommand databaseCommand){ // here we can input name of every object, which implements DatabaseCommand interface
        databaseCommand.select(); // and then choose querry we need
    }

    public void update(DatabaseCommand databaseCommand){
        databaseCommand.update();
    }

    public void insert(DatabaseCommand databaseCommand){
        databaseCommand.insert();
    }

    public void delete(DatabaseCommand databaseCommand){
        databaseCommand.delete();
    }
}
